#!/usr/bin/env python
from sphero_driver import sphero_driver2 as sphero_driver
import sys
import struct
import time
import operator
import threading
import rospy
from sphero_node.msg import VelGroup
from sphero_node.msg import OdometryList
import math
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist
from multiprocessing import Process

MRSP = dict(
  ORBOTIX_RSP_CODE_OK = 0x00,           #Command succeeded
  ORBOTIX_RSP_CODE_EGEN = 0x01,         #General, non-specific error
  ORBOTIX_RSP_CODE_ECHKSUM = 0x02,      #Received checksum failure
  ORBOTIX_RSP_CODE_EFRAG = 0x03,        #Received command fragment
  ORBOTIX_RSP_CODE_EBAD_CMD = 0x04,     #Unknown command ID
  ORBOTIX_RSP_CODE_EUNSUPP = 0x05,      #Command currently unsupported
  ORBOTIX_RSP_CODE_EBAD_MSG = 0x06,     #Bad message format
  ORBOTIX_RSP_CODE_EPARAM = 0x07,       #Parameter value(s) invalid
  ORBOTIX_RSP_CODE_EEXEC = 0x08,        #Failed to execute command
  ORBOTIX_RSP_CODE_EBAD_DID = 0x09,     #Unknown device ID
  ORBOTIX_RSP_CODE_POWER_NOGOOD = 0x31, #Voltage too low for refash operation
  ORBOTIX_RSP_CODE_PAGE_ILLEGAL = 0x32, #Illegal page number provided
  ORBOTIX_RSP_CODE_FLASH_FAIL = 0x33,   #Page did not reprogram correctly
  ORBOTIX_RSP_CODE_MA_CORRUPT = 0x34,   #Main application corrupt
  ORBOTIX_RSP_CODE_MSG_TIMEOUT = 0x35)  #Msg state machine timed out


#ID codes for asynchronous packets
IDCODE = dict(
  PWR_NOTIFY = chr(0x01),               #Power notifications
  LEVEL1_DIAG = chr(0x02),              #Level 1 Diagnostic response
  DATA_STRM = chr(0x03),                #Sensor data streaming
  CONFIG_BLOCK = chr(0x04),             #Config block contents
  SLEEP = chr(0x05),                    #Pre-sleep warning (10 sec)
  MACRO_MARKERS =chr(0x06),             #Macro markers
  COLLISION = chr(0x07))                #Collision detected

RECV = dict(
  ASYNC = [chr(0xff), chr(0xfe)],
  SYNC = [chr(0xff), chr(0xff)])


REQ = dict(
  WITH_RESPONSE =[0xff, 0xff],
  WITHOUT_RESPONSE =[0xff, 0xfe],
  CMD_PING = [0x00, 0x01],
  CMD_VERSION = [0x00, 0x02],
  CMD_SET_BT_NAME = [0x00, 0x10],
  CMD_GET_BT_NAME = [0x00, 0x11],
  CMD_SET_AUTO_RECONNECT = [0x00, 0x12],
  CMD_GET_AUTO_RECONNECT = [0x00, 0x13],
  CMD_GET_PWR_STATE = [0x00, 0x20],
  CMD_SET_PWR_NOTIFY = [0x00, 0x21],
  CMD_SLEEP = [0x00, 0x22],
  CMD_GOTO_BL = [0x00, 0x30],
  CMD_RUN_L1_DIAGS = [0x00, 0x40],
  CMD_RUN_L2_DIAGS = [0x00, 0x41],
  CMD_CLEAR_COUNTERS = [0x00, 0x42],
  CMD_ASSIGN_COUNTER = [0x00, 0x50],
  CMD_POLL_TIMES = [0x00, 0x51],
  CMD_SET_HEADING = [0x02, 0x01],
  CMD_SET_STABILIZ = [0x02, 0x02],
  CMD_SET_ROTATION_RATE = [0x02, 0x03],
  CMD_SET_APP_CONFIG_BLK = [0x02, 0x04],
  CMD_GET_APP_CONFIG_BLK = [0x02, 0x05],
  CMD_SET_DATA_STRM = [0x02, 0x11],
  CMD_CFG_COL_DET = [0x02, 0x12],
  CMD_SET_RGB_LED = [0x02, 0x20],
  CMD_SET_BACK_LED = [0x02, 0x21],
  CMD_GET_RGB_LED = [0x02, 0x22],
  CMD_ROLL = [0x02, 0x30],
  CMD_BOOST = [0x02, 0x31],
  CMD_SET_RAW_MOTORS = [0x02, 0x33],
  CMD_SET_MOTION_TO = [0x02, 0x34],
  CMD_GET_CONFIG_BLK = [0x02, 0x40],
  CMD_SET_DEVICE_MODE = [0x02, 0x42],
  CMD_SET_CFG_BLOCK = [0x02, 0x43],
  CMD_GET_DEVICE_MODE = [0x02, 0x44],
  CMD_RUN_MACRO = [0x02, 0x50],
  CMD_SAVE_TEMP_MACRO = [0x02, 0x51],
  CMD_SAVE_MACRO = [0x02, 0x52],
  CMD_DEL_MACRO = [0x02, 0x53],
  CMD_INIT_MACRO_EXECUTIVE = [0x02, 0x54],
  CMD_ABORT_MACRO = [0x02, 0x55],
  CMD_GET_MACRO_STATUS = [0x02, 0x56],
  CMD_SET_MACRO_STATUS = [0x02, 0x57])

STRM_MASK1 = dict(
  GYRO_H_FILTERED    = 0x00000001,
  GYRO_M_FILTERED    = 0x00000002,
  GYRO_L_FILTERED    = 0x00000004,
  LEFT_EMF_FILTERED  = 0x00000020,
  RIGHT_EMF_FILTERED = 0x00000040,
  MAG_Z_FILTERED     = 0x00000080,
  MAG_Y_FILTERED     = 0x00000100,
  MAG_X_FILTERED     = 0x00000200,
  GYRO_Z_FILTERED    = 0x00000400,
  GYRO_Y_FILTERED    = 0x00000800,
  GYRO_X_FILTERED    = 0x00001000,
  ACCEL_Z_FILTERED   = 0x00002000,
  ACCEL_Y_FILTERED   = 0x00004000,
  ACCEL_X_FILTERED   = 0x00008000,
  IMU_YAW_FILTERED   = 0x00010000,
  IMU_ROLL_FILTERED  = 0x00020000,
  IMU_PITCH_FILTERED = 0x00040000,
  LEFT_EMF_RAW       = 0x00200000,
  RIGHT_EMF_RAW      = 0x00400000,
  MAG_Z_RAW          = 0x00800000,
  MAG_Y_RAW          = 0x01000000,
  MAG_X_RAW          = 0x02000000,
  GYRO_Z_RAW         = 0x04000000,
  GYRO_Y_RAW         = 0x08000000,
  GYRO_X_RAW         = 0x10000000,
  ACCEL_Z_RAW        = 0x20000000,
  ACCEL_Y_RAW        = 0x40000000,
  ACCEL_X_RAW        = 0x80000000)

STRM_MASK2 = dict(
  QUATERNION_Q0      = 0x80000000,
  QUATERNION_Q1      = 0x40000000,
  QUATERNION_Q2      = 0x20000000,
  QUATERNION_Q3      = 0x10000000,
  ODOM_X             = 0x08000000,
  ODOM_Y             = 0x04000000,
  ACCELONE           = 0x02000000,
  VELOCITY_X         = 0x01000000,
  VELOCITY_Y         = 0x00800000)


class many_streaming:

    def __init__(self):
        self.raw_data_buf2 = []
        self.pose_estimation_x = []
        self.pose_estimation_y = []
        self.yaw_estimation = []
        self.bluetooth_dict = {}
        self.data_dict = {}
        self.in_loop_data = {}
        self.data_packet = {}
        self.data_length = {}
        self.run = True
        self.is_connected = True
        self.shutdown = False
        self._communication_lock = threading.Lock()
        self.robot_number = 2
        self.x_data = 0
        self.y_data = 0
        self.z = 0
        self.seq = 0
        self.position = []
        self.time_inital = time.time()
        self.heading = []
        self.speed_list = []
        self.frequency = 8*math.pi
        self.robot_speed = 40 #bits
        self.sphero_number = 3
        self.velocity_subscribe = rospy.Subscriber('xy_values', VelGroup, self.move_spheros)
        self.time_inital = time.time()

    def organize(self, connected_robot_dictionary):
        '''
        Takes in dictioanry of connected robots and determins the names bluetooth locations
        '''
        self.robot_dictionary = connected_robot_dictionary
        self.key_list = list(self.robot_dictionary.keys())
        for i in range(0, len(self.key_list)):
            sphero_name = self.key_list[i]
            self.data_dict[sphero_name] = []
            self.bluetooth_dict[sphero_name] = self.robot_dictionary[sphero_name].robot.bt
        self.btrun(len(self.key_list))


    def btrun(self, sphero_number):
        i = 1
        while not rospy.is_shutdown(): #Infinite loop to run until exciting the terminal
            if i <= sphero_number:
                number = i 
            elif i > sphero_number:
                number = i % sphero_number 
                if number == 0:
                    number = sphero_number
            i = i + 1
            sphero_name = self.key_list[number-1] #determine what sphero this iteration is working with
            self.sphero_name = sphero_name
            try:
              if self.robot_dictionary[sphero_name].robot._communication_lock:
                self.data_dict[sphero_name] += self.bluetooth_dict[sphero_name].recv(1024) # recieve data from the sphero
            except:
              pass

            data = self.data_dict[sphero_name]

            while len(data) > 5: #will run until the length of the data is no longer 5
              try:
                    if data[:2] == RECV['SYNC']: # if the data header is SYNC, run the following lines. Will not publish any data
                        data_length = ord(data[4])
                        if data_length+5 <= len(data):
                            data_packet = data[:(5+data_length)]
                            data = data[(5+data_length):]
                        else:
                            break
         
                    elif data[:2] == RECV['ASYNC']: #If data is ASYNC, run following lines. 
                      data_length = (ord(data[3])<<8)+ord(data[4])

                      if data_length+5 <= len(data): # Data must be greater than 5 to have any meaning. If less than 5, break and move to the next sphero
                          data_packet = data[:(5+data_length)] 
                          data = data[(5+data_length):]
                      else:
                          break
                      '''
                      Determine the message that is being sent. Once the message type is known, unack the data and process it. Then convert it into IMU and odom data in the class
                      '''
                      if data_packet[2]==IDCODE['DATA_STRM'] and self.robot_dictionary[sphero_name].robot._async_callback_dict.has_key(IDCODE['DATA_STRM']):
                          self.robot_dictionary[sphero_name].robot._async_callback_dict[IDCODE['DATA_STRM']](self.robot_dictionary[sphero_name].robot.parse_data_strm(data_packet, data_length))
                      elif data_packet[2]==IDCODE['COLLISION'] and self.robot_dictionary[sphero_name].robot._async_callback_dict.has_key(IDCODE['COLLISION']):
                          self.robot_dictionary[sphero_name].robot._async_callback_dict[IDCODE['COLLISION']](self.robot_dictionary[sphero_name].robot.parse_collision_detect(data_packet, data_length))
                      elif data_packet[2]==IDCODE['PWR_NOTIFY'] and self.robot_dictionary[sphero_name].robot._async_callback_dict.has_key(IDCODE['PWR_NOTIFY']):
                          self.robot_dictionary[sphero_name].robot._async_callback_dict[IDCODE['PWR_NOTIFY']](self.robot_dictionary[sphero_name].robot.parse_pwr_notify(data_packet, data_length))
                    else: #If data is neither SYNC or ASYNC, break and move on. this will happen if residual data remains in the socket
                      data = []
                      self.data_dict[sphero_name] = []
                      break
              except: #If any problems occur, break the loop and move to the next sphero
                  break 
            self.data_dict[sphero_name] = data
           


    def move_spheros(self,data):
        '''
        Subscribing to topic 'xy_values'. 'xy_values' is a list of heading and speed commands. This function will iterate through the commands and publish the data to the spheros
        '''
        speed = data.speed
        heading = data.heading # must be in degrees
        i = 0
        elapsed_time = time.time() - self.time_inital 
        while i < len(self.key_list):
            sphero_name = self.key_list[i]
            self.roll(int(speed[1]), int(heading[1]), 1, False, sphero_name)
            i+=1



    def roll(self, speed, heading, state, response, name):
        """
        This commands Sphero to roll along the provided vector. Both a
        speed and a heading are required; the latter is considered
        relative to the last calibrated direction. A state Boolean is also
        provided (on or off). The client convention for heading follows the 360
        degrees on a circle, relative to the ball: 0 is straight ahead, 90
        is to the right, 180 is back and 270 is to the left. The valid
        range is 0..359.

        :param speed: 0-255 value representing 0-max speed of the sphero.
        :param heading: heading in degrees from 0 to 359.
        :param state: 00h for off (braking) and 01h for on (driving).
        :param response: request response back from Sphero.
        """
        self.send(self.pack_cmd(REQ['CMD_ROLL'],[self.clamp(speed,0,255), (heading>>8), (heading & 0xff), state]), 0, name)


    def send(self, data, response, name):
        """
        Packets are sent from Client -> Sphero in the following byte format::
        -------------------------------------------------------
        | SOP1 | SOP2 | DID | CID | SEQ | DLEN | <data> | CHK |
        ----------------------------------------socket---------------
        * SOP1 - start packet 1 - Always 0xff. 
        * SOP2 - start packet 2 - Set to 0xff when an acknowledgement is\
          expected, 0xfe otherwise.    
        * DID - Device ID
        * CID - Command ID
        * SEQ - Sequence Number - This client field is echoed in the\
          response for all synchronous commands (and ignored by Sphero\
          when SOP2 = 0xfe)
        * DLEN - Data
        * Length - Number of bytes through the end of the packet.
        * <data>
        * CHK - Checksum - The modulo 256 sum of all the bytes from the\
           DID through the end of the data payload, bit inverted (1's\
           complement).
        """
        #compute the checksum
        #modulo 256 sum of data bit inverted
        checksum =~ sum(data) % 256
        #if expecting response back from the sphero
        if response:
          output = REQ['WITH_RESPONSE'] + data + [checksum]
        else:
          output = REQ['WITHOUT_RESPONSE'] + data + [checksum]
        #pack the msg
        msg = ''.join(struct.pack('B',x) for x in output)
        #print('message', msg)
        #send the msg
        with self._communication_lock:# == True and self.is_connected ==True and not self.shutdown:
            self.robot_dictionary[name].robot.bt.send(msg)






    def pack_cmd(self, req ,cmd):
        self.inc_seq()
        #   print req + [self.seq] + [len(cmd)+1] + cmd
        return req + [self.seq] + [len(cmd)+1] + cmd


    def clamp(self, n, minn, maxn):
        return max(min(maxn, n), minn)



    def parrell_motion(self,speed,heading,sphero_name):
        print(sphero_name)
        self.robot_dictionary[sphero_name].robot.roll(int(speed[1]), int(heading[1]), 0, False)


            
    def data2hexstr(self, data):
      return ' '.join([ ("%02x"%ord(d)) for d in data])

    def normalize_angle_positive(self, angle):
        return math.fmod(math.fmod(angle, 2.0*math.pi) + 2.0*math.pi, 2.0*math.pi);

    def inc_seq(self):
        self.seq = self.seq + 1
        if self.seq > 0xff:
          self.seq = 0
