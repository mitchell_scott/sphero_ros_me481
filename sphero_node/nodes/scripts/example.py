#!/usr/bin/env python
import numpy as np
import math
import sys
import time

def normalize_angle_positive(angle):
    return math.fmod(math.fmod(angle, 2.0*math.pi) + 2.0*math.pi, 2.0*math.pi);

def sphero_control(current_time):

    ############# Do not edit above this line ################
    '''
    In this space you can define how you would like the sphero to move. You state left and right motor commands as a function of the time (denoted as current_time).
    You must call the commands the following values: 
    left_power, power to the left motor (0 to 255)
    left_state, state of the motor (0 = off, 1 = forward, 2 = backward, 3 = brake)
    right_power, power to the right motor (0 to 255)
    right_state, state of the motor (0 = off, 1 = forward, 2 = backward, 3 = brake)
    Do not change the names of the values!!
    '''
    #A few notes: The sphero seems to work best at power values between 60 and 90 or so.
    #Remember that the value of power must be between 0 and 255. So if you use a sinusoid you must adjust from -1 to 1 to 0 to 255. You may find the above function normalize_angle_positive useful
    #The following code should cause the sphero to move in a circle. 
    left_power = 80 
    right_power = 0
    left_state = 1
    right_state = 1

    ########### Do not edit below this line ###################

    return [int(left_power), int(right_power), int(left_state), int(right_state)]
