#!/usr/bin/env python
'''
Example script to make the sphero move in a circle
'''



import numpy as np
import math
import sys
import time

import rospy
from sphero_node.msg import MotorPower
 

class robot_move():

    def __init__(self):
        self.x_vel = 0
        self.y_vel = 0
        self.start_time = time.time()
        self.pub = rospy.Publisher('set_motor_commands', MotorPower, queue_size = 10)


    def move_sphero(self):
        rate = rospy.Rate(50)
        while not rospy.is_shutdown(): 
            elapsed_time = time.time() - self.start_time
            velocities = self.sphero_control(elapsed_time) # function located in specified file. Returns [left_power, right_power, left_state, right_state]
            left_power = velocities[0]
            right_power = velocities[1]
            left_state = velocities[2]
            right_state = velocities[3]
            motor_power = MotorPower()
            motor_power.left_power = left_power
            motor_power.left_state = left_state
            motor_power.right_power = right_power
            motor_power.right_state = right_state
            self.pub.publish(motor_power)
            rate.sleep()

        
    
    def stop(self):
        for i in range(0,self.robot_number):
            left_power = 0
            right_power = 0
            left_state = 0
            right_state = 0
            motor_power = MotorPower()
            motor_power.left_power = left_power
            motor_power.left_state = left_state
            motor_power.right_power = right_power
            motor_power.right_state = right_state
            self.pub.publish(motor_power)


    def normalize_angle_positive(self, angle):
        return math.fmod(math.fmod(angle, 2.0*math.pi) + 2.0*math.pi, 2.0*math.pi);


    def sphero_control(self, current_time):
    
        left_power = 80 
        right_power = 0
        left_state = 1
        right_state = 1
    
        return [int(left_power), int(right_power), int(left_state), int(right_state)] 


if __name__ == '__main__':
    rospy.init_node('move', anonymous=True)
    r = robot_move()

    try:
        r.move_sphero()
    except KeyboardInterrupt:
        stop = r.stop()
        if stop:
            sys.exit(1)
        else:
            stop = r.stop()
            sys.exit(1)
