sphero_ros Edited by Mitchell Scott for usage in ME481
Original by Melonee Wise
==========

checkout the [docs](http://mmwise.github.com/sphero_ros)  

=======
## Installation
You may want perform the following steps in a separate workspace, instead of the one you were originally working with. Simply removing the original **sphero_ros** folder prior to performing the steps will also work, but is not a best practice.
*Note*: <catkin_ws> denotes location of your catkin workspace. 

      $ cd <catkin_ws>/src
      $ git clone https://mitchell_scott@bitbucket.org/mitchell_scott/sphero_ros_me481.git
      $ cd <catkin_ws>
      $ catkin_make
      $ echo "source ~/catkin_ws/devel/setup.bash" >> ~/.bashrc
      ...

If errors persist (such as cannont import name sphero_driver) run:  
source ~<catkin_ws>/devel/setup.bash on all new terminal  
=======
## To Use

First, navigate to sphero_node/nodes.  
Second, you must tell the node what sphero you want to use. Navigate to sphero_node/nodes/scripts/run_params.py and edit the target_sphero line to the sphero you are using. This line is very important!!   If you put the wrong sphero then you may connect to your neighbor!  
Third, run:
	
```
#!python

$ python sphero_start.py
```

That command will attempt to locate the sphero and connect via bluetooth. It will also run the ros bindings. The sphero should now be solid green in color. If it is not, run the sphero_start script again until it works.

To run in terminal run:  
       Put the sphero on the ground! It's about to move.
       
```
#!python

$ rostopic pub /set_motor_commands sphero_node/MotorPower
```
 and then hit 'tab' twice. You will then see something like:  
	"left_power: 0.0  
	left_state: 0  
	right_power: 0.0  
	right_state: 0"  
       Replace the power commands with 60 and the state commands with 1 like so:   
	
```
#!python

rostopic pub /set_motor_commands sphero_node/MotorPower
```
 and then hit 'tab' twice. You will then see something like:
	"left_power: 60.0
	left_state: 1.0
	right_power: 60.0
	right_state: 1.0"
       The sphero will now move in a straight line for 1 second. To make it run for longer add -r 10 after pub to have it run at 10 Hz. 



You can now send commands to the sphero. 
An example python script to control the sphero via. motor commands is shown in scripts/move_spheros.py. To run move_spheros.py:
	Put the sphero on the ground! It's about to move.
	
```
#!python

$ python move_sphero.py
```


The sphero should now move in a circle.
